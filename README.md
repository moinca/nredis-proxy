nredis-proxy 1.0.1 版本优化以下功能点

1：修改netty atrribute 并发问题
2：优化tcp参数
3：优化连接池,升级netty最新版本，使单机器整体性能损耗最高在14%左右

nredis-proxy 1.0.2 版本优化以下功能点
1：修复高并发环境IO泄露，造成 open too many files
2：去掉front channel 与back channel 重量级同步锁以及循环链表算法，使用cpu级别volatile，精简逻辑流程
3：修复 TCP丢包问题
4：单机单个redis性能大概在QPS：9千左右 

nredis-proxy 1.0.2.1 版本修改bug
1: multiBulkReply 超过50 数据量大,出现重复数据问题，已经解决，通过两天暴力测试，没有任何问题
2: 提供linux 启动命令

CPU性能:
 
 ![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/165825_009f1558_54128.jpeg "在这里输入图片标题")

内存性能:
  
 
![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/165858_ab9bde06_54128.jpeg "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/165908_82918698_54128.jpeg "在这里输入图片标题")

线程以及classloader性能:

  ![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/165929_35b0aae6_54128.jpeg "在这里输入图片标题")

RedisServer性能监控

   ![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/172008_783ff6e8_54128.jpeg "在这里输入图片标题")
   ![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/172021_141e70f5_54128.jpeg "在这里输入图片标题")
   ![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/172032_5085bc9d_54128.jpeg "在这里输入图片标题")

RedisServer 主从自动切换监控

   ![输入图片说明](http://git.oschina.net/uploads/images/2017/0115/172154_e402067a_54128.jpeg "在这里输入图片标题")